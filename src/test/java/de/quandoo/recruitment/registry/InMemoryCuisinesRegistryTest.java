package de.quandoo.recruitment.registry;

import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import de.quandoo.recruitment.registry.api.CuisineRegistryException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.text.MessageFormat;
import java.util.List;

public class InMemoryCuisinesRegistryTest {

	private static final Cuisine frenchCuisine = new Cuisine("french");
	private static final Cuisine germanCuisine = new Cuisine("german");
	private static final Cuisine italianCuisine = new Cuisine("italian");
	private InMemoryCuisinesRegistry cuisinesRegistry;

	@Before
	public void initTestedObject() {
		cuisinesRegistry = new InMemoryCuisinesRegistry(newHashSet(frenchCuisine, germanCuisine, italianCuisine));
	}

	@Test
	public void shouldRegisterCuisinesAndProvideStatistics() {
		final Customer customer1 = new Customer("1");
		final Customer customer2 = new Customer("2");
		final Customer customer3 = new Customer("3");

		//register cuisines
		cuisinesRegistry.register(customer1, frenchCuisine);
		cuisinesRegistry.register(customer1, germanCuisine);
		cuisinesRegistry.register(customer1, italianCuisine);

		cuisinesRegistry.register(customer2, germanCuisine);
		cuisinesRegistry.register(customer2, italianCuisine);
		cuisinesRegistry.register(customer3, italianCuisine);
		//already registered, should be skipped
		cuisinesRegistry.register(customer3, italianCuisine);

		//verify customers
		verifyObjectsInList(cuisinesRegistry.cuisineCustomers(frenchCuisine), customer1);
		verifyObjectsInList(cuisinesRegistry.cuisineCustomers(germanCuisine), customer1, customer2);
		verifyObjectsInList(cuisinesRegistry.cuisineCustomers(italianCuisine), customer1, customer2, customer3);

		//verify cuisines
		verifyObjectsInList(cuisinesRegistry.customerCuisines(customer1), frenchCuisine, germanCuisine, italianCuisine);
		verifyObjectsInList(cuisinesRegistry.customerCuisines(customer2), germanCuisine, italianCuisine);
		verifyObjectsInList(cuisinesRegistry.customerCuisines(customer3), italianCuisine);

		//verify top cuisines order
		final List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(2);
		verifyListSize(topCuisines, 2);
		assertThat(topCuisines.get(0), is(italianCuisine));
		assertThat(topCuisines.get(1), is(germanCuisine));
	}

	@Test(expected = CuisineRegistryException.class)
	public void shouldNotRegister_unknownCuisine() {
		cuisinesRegistry.register(new Customer("1"), new Cuisine("Something nonexistent"));
	}

	@Test
	public void shouldReturnEmptyLists_noCuisinesRegistered() {
		verifyListSize(cuisinesRegistry.customerCuisines(new Customer("1")), 0);
		verifyListSize(cuisinesRegistry.cuisineCustomers(new Cuisine("1")), 0);
	}

	/**
	 * Verify content of result list
	 *
	 * @param list            list
	 * @param expectedObjects array of expected objects in any order
	 * @param <T>             type of objects
	 */
	private static <T> void verifyObjectsInList(List<T> list, T... expectedObjects) {
		verifyListSize(list, expectedObjects.length);
		for (T object : expectedObjects) {
			assertThat(MessageFormat.format("Expected object {0} is not found in result list", object), list, hasItem(object));
		}
	}

	/**
	 * Verify size of result list
	 *
	 * @param list         list
	 * @param expectedSize expected size
	 */
	private static void verifyListSize(List list, int expectedSize) {
		assertThat(list, notNullValue());
		assertThat(list.size(), is(expectedSize));
	}

}