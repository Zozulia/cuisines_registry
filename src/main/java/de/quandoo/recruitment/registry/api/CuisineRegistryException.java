package de.quandoo.recruitment.registry.api;

/**
 * Cuisine registry's common runtime exception
 *
 * @author Dmytro Zozulia
 */
public class CuisineRegistryException extends RuntimeException {
	public CuisineRegistryException() {
	}

	public CuisineRegistryException(String message) {
		super(message);
	}

	public CuisineRegistryException(String message, Throwable cause) {
		super(message, cause);
	}

	public CuisineRegistryException(Throwable cause) {
		super(cause);
	}

	public CuisineRegistryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
