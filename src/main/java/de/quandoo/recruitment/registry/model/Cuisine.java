package de.quandoo.recruitment.registry.model;

import lombok.Data;

@Data
public class Cuisine {

	private final String name;

}
