package de.quandoo.recruitment.registry.model;

import lombok.Data;

@Data
public class Customer {

	private final String uuid;

}
