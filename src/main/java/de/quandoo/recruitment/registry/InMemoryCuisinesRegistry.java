package de.quandoo.recruitment.registry;

import static com.google.common.base.Verify.verifyNotNull;
import static com.google.common.collect.Maps.newConcurrentMap;
import static com.google.common.collect.Sets.newHashSet;

import de.quandoo.recruitment.registry.api.CuisineRegistryException;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * Implementation of {@link CuisinesRegistry}, uses {@link ConcurrentMap} instances to store customer-cuisines and cuisine-customers relations
 */
@Slf4j
@Data
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	private static final Comparator<Set<Customer>> topCuisinesComparator = createTopCuisinesComparator();

	private final Map<Customer, Set<Cuisine>> customerCuisinesMap = newConcurrentMap();
	private final Map<Cuisine, Set<Customer>> cuisineCustomersMap = newConcurrentMap();
	private final Set<Cuisine> availableCuisines;


	@Override
	public void register(final Customer customer, final Cuisine cuisine) {
		verifyNotNull(cuisine, "Cuisine is null");
		verifyNotNull(customer, "Customer is null");
		log.trace("Registering {} for a customer id {}", cuisine.getName(), customer.getUuid());

		if (!availableCuisines.contains(cuisine)) {
			throw new CuisineRegistryException(MessageFormat.format("Cannot complete cuisine registration for a " +
					"customer {0}, provided cuisine {1} does not exists in repository.", customer, cuisine));
		}

		customerCuisinesMap.compute(customer, getRelationsUpdateFunction(cuisine));
		cuisineCustomersMap.compute(cuisine, getRelationsUpdateFunction(customer));
		log.trace("Finish registering {} for a customer id {}", cuisine.getName(), customer.getUuid());
	}

	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		verifyNotNull(cuisine, "Cuisine is null");
		log.trace("Retrieving customers of {}", cuisine.getName());

		final Set<Customer> customers = cuisineCustomersMap.get(cuisine);
		if (customers == null) {
			log.debug("No customers found for {}", cuisine.getName());
			return Collections.emptyList();
		}

		log.debug("Found {} customers of {}", customers.size(), cuisine.getName());
		return Collections.unmodifiableList(new ArrayList<>(customers));
	}

	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		verifyNotNull(customer, "Cuisine is null");
		log.trace("Retrieving cuisines for customer id {}", customer.getUuid());

		final Set<Cuisine> cuisines = customerCuisinesMap.get(customer);
		if (cuisines == null) {
			log.debug("No cuisines found for customer id {}", customer.getUuid());
			return Collections.emptyList();
		}

		log.debug("Found {} cuisines for customer id {}", cuisines.size(), customer.getUuid());
		return Collections.unmodifiableList(new ArrayList<>(cuisines));
	}

	@Override
	public List<Cuisine> topCuisines(final int n) {
		log.trace("Retrieving top {} cuisines", n);
		final List<Cuisine> sortedCuisines = cuisineCustomersMap.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(topCuisinesComparator))
				.limit(n)
				.map(Map.Entry::getKey).collect(Collectors.toList());

		return Collections.unmodifiableList(sortedCuisines);
	}

	/**
	 * Create a function to add a new element to a set of objects in relations represented by {@code Map<Object,Set>}.
	 * The function creates a new Set if there are no relations exist.
	 *
	 * @param element object to add to relation
	 * @param <T1>    type of key object
	 * @param <T2>    type of related object to be added
	 * @return new function
	 */
	private static <T1, T2> BiFunction<T1, Set<T2>, Set<T2>> getRelationsUpdateFunction(final T2 element) {
		return (keyObject, relatedObjects) -> {
			if (relatedObjects == null) {
				relatedObjects = newHashSet();
			}
			relatedObjects.add(element);
			return relatedObjects;
		};
	}

	/**
	 * Create comparator for top cuisines which sorts entries of {@link InMemoryCuisinesRegistry#cuisineCustomersMap}
	 * by number of customers in descending order
	 *
	 * @return comparator
	 */
	private static Comparator<Set<Customer>> createTopCuisinesComparator() {
		return (o1, o2) -> {
			if (o1.size() == o2.size()) {
				return 0;
			} else if (o1.size() > o2.size()) {
				return -1;
			}
			return 1;
		};
	}
}
